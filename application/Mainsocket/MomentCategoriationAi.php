<?php
namespace Mf\Mainsocket;

Use Unirest\Request\Body;
Use Unirest\Request;
use \Mf_Core\Registry;


Class MomentCategoriationAi {

	protected $_MomentDb;



	public function __construct($dataForMomentCategorisation)
	{
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_MomentDb = Registry::getInstance()->get('MomentDb');
		if(strlen($dataForMomentCategorisation["caption"]) > 1){
		   $this->execute($dataForMomentCategorisation["momentId"], $dataForMomentCategorisation["caption"]);
	  }
	}



	private function execute($momentId, $caption)
	{
			$headers = array('Accept' => 'application/json', 'X-Mashape-Key' => 'BJ2MsNGbiymshYkQ8Zz2Ja2KxWUwp1CFy8ojsnYVGZTbNbrkbm');
			$data = array('entry' => $caption);

			$body = Body::form($data);

			$response = Request::post('https://twinword-word-associations-v1.p.mashape.com/associations/', $headers, $body);

			$topics = @json_decode($response->raw_body, true)['associations_array'];
			if(!is_null($topics)) {
					$foundTopics = [];
					foreach ($topics as $key) {
							$foundTopics[] = $key;
					}


					$strip_caption = str_replace('"', "-", str_replace("'", "-", $caption));

					$split = explode(' ', $strip_caption);

					foreach ($split as $s) {
							$foundTopics[] = $s;
					}

					// Match Interest with found categories...
					$match = $this->_MomentDb->matchInterests($momentId, $foundTopics);

					if ($match) {
							$this->_response = array('status' => 'ok', 'message' => 'Moment Categorized');
					} else {
							$this->_response = array('status' => 'ok', 'message' => 'Moment Randomized');
					}
			} else {
						 $this->_response = array('status' => 'ok', 'message' => 'NLP returned no associations, killing process...');
			}



	}




}//ends class



?>
