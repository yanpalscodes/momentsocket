<?php
namespace Mf\Mainsocket;
use \GearmanClient;
use \GearmanTask;
use Mf\WebSocket\Event\Event;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;
use Mf\Mainsocket\library\Mainsocket\GeneralFunctions;
use Mf\Mainsocket\library\Mainsocket\Moment;
use Mf\Mainsocket\library\Mainsocket\YanPalUser;
Use Unirest\Request\Body;
Use Unirest\Request;


Class CreateMomentController {

	protected $event;
	protected $message;
	protected $_UserDb;
	protected $_MomentDb;
	protected $_CommentDb;
	public $returnedData = array();



	public function __construct()
	{
		$this->_UserDb = Registry::getInstance()->get('UserDb');
		$this->_MomentDb = Registry::getInstance()->get('MomentDb');
	}


	public function execute($message, $event)
	{
		$startTime = microtime(true);
		$server = $event->getTarget();
		$client = $event->getParam('client');
		$this->generalFunctions = new GeneralFunctions();
		///$server->log(json_encode($message));
		if(!isset($message->userId) ||!isset( $message->caption)){
			$this->returnedData  = array(
				"msg" => array("status" => "error", "payload"=>"", "message"=>"incomplete data", "controller" => "moment",
				"action" => "createMoment"),
				"users" => array(),
				"dataForMomentCategorisation" => array('caption' => "", "momentId"=>NULL)
			);
		}
		else{
		$momentId = $this->generalFunctions-> IdGenerator();
		$userId = $message->userId;
		$this->_caption = $message->caption;
		$location = null;
		if($message->latitude != null || $message->latitude != ""){
			$location = json_encode(array("latitude"=>$message->latitude, "longitude"=>$message->longitude));
		}
		$data = array("MomentId"=>$momentId, "UserId"=>$userId, "Content"=>$this->_caption, "Time"=>time(), "Location"=>$location);

		if($this->_MomentDb->storeMoment($data)){
				$this->storeMomentMedia($message->media, $momentId, $userId);
		}
	}
}


	public function storeMomentMedia($media, $momentId, $userId)
	{

		foreach($media as $oneMedium){

		$mediaId = $this->generalFunctions->IdGenerator();
		$fileName = $this->getFileName($oneMedium->url);
		$posterUrl = $this->getFileName($oneMedium->posterUrl);
		$type = $this->typeSwitcher($oneMedium->type);
		$data  = array('UserId' => $userId, "MediaId"=>$mediaId, "Type"=>$type, "MediaName"=>$fileName,  "Cropped"=>0,
		"Trashed"=>0, "Source"=>1, "SourceId" => $momentId, "Time"=>time(), "Poster"=>$posterUrl);
		$this->_MomentDb->storeMedia($data);
		}


		$justCreatedMoment =  $this->_MomentDb->getMomentByMomentId($momentId);
		$justCreatedMoment = new Moment($justCreatedMoment, $userId, false);//false means it will not fetch comments

		$userFollowersIds  = array();
		$usersFollowersFromDd = $this->_UserDb->fetchUserFollowers($userId);

		foreach ($usersFollowersFromDd as $key) {
			$userFollowersIds[] = $key["FollowerId"];
		}

		$this->returnedData  = array(
			"msg" => array("status" => "ok", "payload"=>$justCreatedMoment, "message"=>"moment created", "controller" => "moment",
			"action" => "createMoment"),
			"users" => $userFollowersIds,
			"dataForMomentCategorisation" => array('caption' => $this->_caption, "momentId"=>$momentId)
		);


	}


	private 	function getFileName($url)
	{
		$guid = explode("/",$url);
		return  $guid[count($guid)-1];
	}


	private function typeSwitcher($oneMedium)
	{
	 if($oneMedium == "image"){
		 $type = 1;
	 }
	 if($oneMedium == "video"){
		 $type = 2;
	 }
	 return $type;
	}














	public function build()
	{
		return $this->returnedData;
	}


}//ends class



?>
