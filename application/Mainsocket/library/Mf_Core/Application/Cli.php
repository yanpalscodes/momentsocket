<?php
/**
 *
 */
namespace Mf_Core\Application;

use \Exception;
use Mf_Core\Application\Application;

/**
 * Base class for Cli Applications
 *
 * @package
 */
abstract class Cli extends Application
{
	/**
	 * Command-line arguments
	 */
	protected $_argv;

	/**
	 * Number of command-line arguments
	 */
	protected $_argc;

	/**
	 *
	 */
	public function __construct()
	{
		if($this->getSapi() !== 'cli')
		{
			throw new Exception("INVALD_APPLICATION_ENVIRONMENT");
		}

		$this->_argc = $_SERVER['argc'];
		$this->_argv = $_SERVER['argv'];
	}
}