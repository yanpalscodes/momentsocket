<?php
/**
 *
 */
namespace Mf_Core\Mvc;

use \Exception;
use \ReflectionClass;
use \Mf_Core\Mvc\Controller;

/**
 * Mf_Core\View
 *
 * FUTURE VERSION
 * - View should use a Templating engine
 */
abstract class View
{
	/**
	 * Name of view
	 */
	protected $_name;

	/**
	 * Path to loaded view
	 */
	protected $_path;

	/**
	 * Filename
	 */
	protected $_file;

	/**
	 *
	 */
	protected $_format;

	/**
	 * \Mf_Core\Model
	 */
	protected $_model;

	/**
	 * The output of the template script.
	 *
	 * @var string
	 */
	protected $_output;

	/**
	 *
	 */
	public function __construct()
	{
		// Dynamically find controller path & name
		$r = new ReflectionClass($this);
		$this->_file = $r->getFileName();
		$this->_path = dirname($r->getFileName());
		$this->_name = $r->getName();
	}

	/**
	 *
	 */
	public function getModel()
	{
		$controller = Controller::getInstance();
		return $controller->getModel();
	}

	/**
	 *
	 */
	public function escape($data)
	{
		// return htmlentities($data, ENT_COMPAT, 'UTF-8');
		return filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
	}

	/**
	 *
	 */
	public function getPath()
	{
		return $this->_path;
	}

	/**
	 *
	 */
	public function getFile()
	{
		return $this->_file;
	}

	/**
	 *
	 */
	public function getName()
	{
		return $this->_name;
	}

	/**
	 *
	 */
	protected function _loadTemplate($tmpl)
	{
		$this->_output = null;
		$tmplPath = $this->getPath() . '/tmpl/' . $tmpl . '.tmpl.php';

		if(file_exists($tmplPath))
		{
			ob_start();
			require $tmplPath;
			$this->_output = ob_get_contents();
			ob_get_clean();
		}
		else {
			throw new Exception("EXCEPTION_VIEW_TMPL_NOT_FOUND", 404);
		}

		return $this->_output;
	}

	/**
	 *
	 */
	public function setOutput($output)
	{
		$this->_output = $output;
	}

	/**
	 *
	 */
	public function getOutput()
	{
		return $this->_output;
	}

	/**
	 *
	 */
	public function display()
	{
		echo $this->_output;
	}
}