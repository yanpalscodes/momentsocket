<?php
/**
 *
 */
namespace Mf_Core\Mvc\View;

use \Exception;
use \Mf_Core\Mvc\Controller;
use \Mf_Core\Mvc\View;
use \Mf_Core\Mvc\View\Layout;

/**
* 
*/
abstract class Html extends View
{
	//
	protected $_layout;
	
	//
	public function __construct()
	{
		parent::__construct();
		$format = 'html';
	}

	//
	public function setLayout($path, array $options = array())
	{
		$this->_layout = Layout::getInstance($path, $options);
	}

	//
	public function getLayout()
	{
		return $this->_layout;
	}

	/**
	 *
	 */
	public function display($tmpl = 'default')
	{
		$this->_loadTemplate($tmpl);

		// Modify output if theme is set
		if($this->getLayout())
		{
			// Set layout content
			$this->getLayout()->loadModule('content', $this->getOutput());
			$this->setOutput($this->_layout->render());
		}

		parent::display();
	}
}