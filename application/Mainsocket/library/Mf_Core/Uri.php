<?php
/**
 *
 */
namespace Mf_Core;

use Exception;

/**
 * Uri
 *
 * Works with urls, not URIs
 */
class Uri
{
	/**
	 *
	 */
	protected $_scheme;

	/**
	 *
	 */
	protected $_host;

	/**
	 *
	 */
	protected $_port;

	/**
	 *
	 */
	protected $_user;

	/**
	 *
	 */
	protected $_pass;

	/**
	 *
	 */
	protected $_path;

	/**
	 *
	 */
	protected $_query;

	/**
	 *
	 */
	protected $_fragment;

	/**
	 * Uri object representing the current url
	 */
	protected static $_instance;

	/**
	 * Base path of the url e.g '/', '/public'
	 *
	 * For non-current Uri, the base is can be 
	 */
	protected $_base;
	
	/**
	 *
	 */
	public function __construct($url, $base = '/')
	{
		$props = static::parse($url);
		$this->setProperties($props);

		$this->_base = $base;
	}

	/**
	 * Parses a url string and returns its properties
	 *
	 * @return array 	Url properties
	 */
	public static function parse($url)
	{
		if(!($parts = parse_url($url)) || !filter_var($url, FILTER_VALIDATE_URL))
		{
			throw new Exception('Invalid Url');
		}

		$url = urlencode($url);

		return $parts;
	}

	/**
	 * @return 	array 	Properties modified
	 */
	public function setProperties(array $options)
	{
		$modified = array();

		foreach($options as $key => $value)
		{
			$property = '_'.$key;
			
			if(property_exists(__CLASS__, $property))
			{
				array_push($modified, $property);

				$this->setProperty($property, $value);
			}
		}

		return $modified;
	}

	/**
	 * Sets a uri property
	 */
	public function setProperty($property, $value)
	{
		$property = strpos($property,'_') !== 0 ? '_'.$property:$property;

		if(property_exists(__CLASS__, $property))
		{
			$this->{$property} = $value;

			return true;
		}

		return false;
	}

	/**
	 * Returns a uri property
	 */
	public function getProperty($property, $default = null)
	{
		$property = strpos($property,'_') !== 0 ? '_'.$property:$property;

		if(property_exists(__CLASS__, $property))
		{
			return $this->{$property};
		}

		return $default;
	}

	/**
	 * Creates and returns a URI instance of the requested url
	 * or current url.
	 *
	 * The current url is single.
	 *
	 * @return Uri
	 */
	public static function getInstance()
	{
		if(!static::$_instance)
		{
			$url = [];
			$url['scheme'] = isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off') ? 'https://' : 'http://';
			$url['host'] = $_SERVER['HTTP_HOST'];
			$url['port'] = $_SERVER['SERVER_PORT'] != '80' ? $_SERVER['SERVER_PORT'] : '';
			$url['path'] = $_SERVER['REQUEST_URI']; // Request URI

			// Get the base path
			$scriptPath = isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : $_SERVER['PHP_SELF'];
			$scriptName = basename($scriptPath);
			$base = str_replace(DIRECTORY_SEPARATOR, '/', substr($scriptPath, 0, stripos($scriptPath, $scriptName)));

			// Trim trailing forward slash
			if(strlen($base) > 1)
			{
				$base = rtrim($base);
			}

			// Create url
			static::$_instance = new self(implode(array_values($url)), $base);
		}

		return self::$_instance;
	}

	/**
	 *
	 */
	public function getBase()
	{
		return $this->_base;
	}

	/**
	 * Returns if a uri has a property set
	 */
	public function hasProperty($property)
	{
		// isset
		if(property_exists(__CLASS__, $property) && !empty($this->{$property}))
		{
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	public function toString()
	{
		$props = array('scheme', 'host', 'port', 'path', 'query', 'fragment');
		$url = '';

		foreach($props as $prop)
		{
			$prop = '_'.$prop;
			if($this->hasProperty($prop))
			{
				switch ($prop) {
					case '_scheme':
						$url .= $this->getProperty($prop) . '://';
						break;

					case '_port':
						$url .= ':' . $this->getProperty($prop);
						break;

					case '_query':
						$url .= '?' . $this->getProperty($prop);
						break;

					case '_fragment':
						$url .= '#' . $this->getProperty($prop);
						break;
					
					default:
						$url .= $this->getProperty($prop);
						break;
				}
			}
		}

		return urldecode($url);
	}

	/**
	 *
	 */
	public function __toString()
	{
		return $this->toString();
	}

	/**
	 *
	 */
	public function getPath()
	{
		return $this->_path;
	}
}
