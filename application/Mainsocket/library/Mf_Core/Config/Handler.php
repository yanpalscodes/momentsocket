<?php
/**
 *
 *
 */
namespace Mf_Core\Config;

use Exception;

/**
* 
*/
abstract class Handler
{
	/**
	 *
	 */
	protected $_type;

	/**
	 *
	 */
	protected function __construct()
	{
		// Perform compatibility check
		// E.g if(function_exists('parse_ini_file'))
	}

	/**
	 * Factory method to create handler
	 * Creates a ConfigHandler
	 *
	 * @param 	$type 	string
	 *
	 * @return Mf_Core\Config\Handler
	 */
	public static function createHandler($type = 'Php')
	{
		$classname = __NAMESPACE__ . '\\' . ucfirst($type);

		if(!class_exists($classname))
		{
			throw new Exception("Configuration parser is missing", 500);
		}

		$handler = new $classname;

		return $handler;
	}

	/**
	 * Reads and converts data contained in file into
	 * an object
	 *
	 * @return 	object
	 */
	abstract public function parse($file);
}
