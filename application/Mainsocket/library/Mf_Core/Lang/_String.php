<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use Mf_Core\Lang\TypeAbstract;

/**
 * String datatype
 */
class _String extends TypeAbstract
{
	/**
	 * Internally attempts to convert var to string
	 *
	 * @param 	$var 	mixed
	 * @throws 	InvalidArgumentExceptionException	Can't convert var
	 */
	public function __construct($var)
	{
	}
}