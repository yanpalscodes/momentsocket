<?php
/**
 *
 */
namespace Mf_Core\Lang; 

use \InvalidArgumentException;
use Mf_Core\Lang\TypeAbstract;

/**
 * Boolean datatype wrapper
 */
class _Boolean extends TypeAbstract
{
	/**
	 * Converts var to falsy or truthy value
	 * null, 0, '', undefined are falsy.
	 *
	 * @param 	$var 	mixed
	 */
	public function __construct($var)
	{
		$this->_value = !!$var;
	}
	
	/**
	 *
	 */
	public function toString()
	{
		return (string) $this->getValue();
	}
}