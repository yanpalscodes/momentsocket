<?php
/**
 *
 */
namespace Mf_Core\Http;

/**
 * 
 */
class Response
{
	// 2x
	const HTTP_STATUS_OK = 200;
	// 3x
	const HTTP_STATUS_MOVED_PERMANENTLY = 301;
	const HTTP_STATUS_SEE_OTHER = 303;
	const HTTP_STATUS_NOT_MODIFIED = 304;
	const HTTP_STATUS_TEMPORARY_REDIRECT = 307;
	// 4x
	const HTTP_STATUS_BAD_REQUEST = 404;
	const HTTP_STATUS_FORBIDDEN = 403;
	const HTTP_STATUS_NOT_FOUND = 404;
	// 5x
	const HTTP_STATUS_INTERNAL_SERVER_ERROR = 500;
	const HTTP_STATUS_BAD_GATEWAY = 502;
	const HTTP_STATUS_SERVICE_UNAVAILABLE = 503;

	/**
	 *
	 */
	public function __construct()
	{
		# code...
	}
}