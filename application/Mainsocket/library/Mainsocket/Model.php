<?php
/**
 *
 */
use Mf_Core\Model;

/**
 * BaseController
 *
 * This doesn't save any request. Hence it must be abstract and must not 
 * provide any action method.
 */
abstract class WokonView extends Model
{
}
