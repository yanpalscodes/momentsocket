<?php

use \Mf_Core\Registry;
use \Mf_Core\Session;
use \Mf_Core\Config;



defined('_PUBLIC') || exit;

class RebroadcastHelper{

	public $momentId;
	public $content;
	public $media = NULL;
	public $time;
	public $userId;
	public $isOwner;
	public $rebroadcastCount = 0;
	public $userInfo;




	public function __construct($options, $loggedInUserId)
	{
		$this->momentId = isset($options['MomentId']) ? $options['MomentId'] : NULL;
		$this->time = isset($options['Time']) ? $options['Time'] : NULL;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : NULL;
		$this->isOwner = ($loggedInUserId == $this->userId) ? true : false;
		$this->content = isset($options['Content']) ? $options['Content'] : NULL;
		$this->countRebroadcast($this->momentId);
		$this->userInfo = $this->_getUser();
		$this->arrangeMedia($this->momentId);
	}




	public function  getRebroadcast()
	{
		$rebArray = array("momentId"=>$this->momentId, "content"=>$this->content,"media"=>$this->media,"time"=>$this->time, "userId"=>$this->userId,
		  "isOwner"=>$this->isOwner,  "rebroadcastCount"=>$this->rebroadcastCount, "userInfo"=>$this->userInfo);
		return $rebArray;
	}


	public function get($key){
		$newKey = (strpos($key, '_') === false) ? '_'.$key : $key;
		if(property_exists(__CLASS__, $newKey)){
			return $this->{$newKey};
		}
		return false;
	}

	private function countMomentComments(){
			return "";
	}


		private function _getUser()
		{
			$this->_db = new DatabaseUser;
			$userInfo = $this->_db->fetchUser($this->userId);
			$userInfo =  get_object_vars($userInfo);
			if(is_array($userInfo)){
				return new YanPalUser($userInfo);
			}
			return;
		}





	private function countRebroadcast($momentId)
	{

		$db = new  DatabaseMoments;
		$this->rebroadcastCount = $db->countNumberOfRebroadcastForAMoment($momentId);
	}



	private function arrangeMedia($momentId)
	{

			$db = new DatabaseMoments();
			$mediaDetails = $db->getMediaByMomentId($momentId);
			$this->continueToArrangeMedia($mediaDetails);

	}

	private function continueToArrangeMedia($mediaDetails)
	{

		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');
		$media = array();
		foreach($mediaDetails as $one){
			$oneMedia = array();
		  $oneMedia["type"] = $this->typeSwitcher($one["Type"]);
			$oneMedia["mediaId"] = $one["MediaId"];
			//now append url from config
			$oneMedia["url"] = ($oneMedia["type"] == "image")? $uploadsConfig->originalPhotoUrl.$one["MediaName"] :
			$uploadsConfig->videoUrl.$this->getVideoFolderNameAnDVideoName($one["MediaName"]) ;

			//now compose url for the dash file
			$oneMedia["mpdUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getDashUrl($one["MediaName"]): NULL;

			//now compose url for the segment
			$oneMedia["segmentInitUrl"] = ($oneMedia["type"] == "video")?  $uploadsConfig->videoUrl.$this->getSegmentUrl($one["MediaName"]): NULL;


			$oneMedia["loopUrl"] = ($oneMedia["type"] == "video")?  $this->getLoopUrl($one) : NULL;
			$oneMedia["poster"] =  $uploadsConfig->posters.$one["Poster"];
			$media[] = $oneMedia;
		}
		$this->media = $media;
	}




	public function getLoopUrl($one)
	{
		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');

				$file = $uploadsConfig->loopUrl.$one["MediaName"];
		$file_headers = @get_headers($file);
		if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
				return NULL;
		}
		else {
					return $uploadsConfig->loopUrl.$one["MediaName"];
		}

	}




	private function getVideoFolderNameAnDVideoName($videoFileName)
	{
		$foldername = explode(".",$videoFileName);
		return $foldername[0]."/".$videoFileName;
	}

	private function getDashUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/". $foldername[0]  ."_encoded_dash.mpd";
	}

	private function getSegmentUrl($fileName)
	{
		$foldername = explode(".",$fileName);
		return $foldername[0]."/"."segment_init.mp4";
	}

	private function typeSwitcher($type)
	{
		if($type == 1){
			return "image";
		}
		elseif($type == 2)
		{
				return "video";
		}
	}

	public function arrangeContent($content, $loggedInUserId, $userId)
	{

	}
}//end  of class
