<?php
namespace Mf\Mainsocket\library\Mainsocket;
// Direct access check
defined('_PUBLIC') || exit;
use Mf_Core\Database;
use \Mf_Core\Registry;
use \Mf_Core\Config\Config;


class YanPalUser
{
	public $userId;
	public $firstName;
	public $lastName;
	public $gender;
	public $day;
	public $month;
	public $year;
	public $birthday;
	public $email;
	public $phone;
	public $settings;
	public $onlineStatus;
	public $accountStatus;
	public $device;
	public $username;
	public $accountType;
	public $dateJoined;
	public $website;
	public $urlName;
	public $cityId;
	public $stateId;
	public $countryId;
	public $bioInfo;
	public $city;
	public $state;
	public $country;
	public $userCoverMedia;
	public $avatar;
	public $userCoverMediaType;
	public $posterUrl;
	public $numbersOfFollowers;
	public $hasAvatar;
	public $location;
	public $verificationStatus;
	private $_db;


	/**
	 *
	 */
	protected static $_instance;

	/**
	 * Creates instance of User
	 */
	public function __construct($options)
	{
		$this->_db =  Registry::getInstance()->get('UserDb');
		$options = (is_bool($options))? array(): $options;
		$this->userId = isset($options['UserId']) ? $options['UserId'] : 0;
		$this->firstName = isset($options['FirstName']) ? ucfirst($options['FirstName']) : '';
		$this->lastName = isset($options['LastName']) ? ucfirst($options['LastName']) : '';
		$this->gender = isset($options['Gender']) ? $options['Gender'] : '';
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : '';
		$this->email = isset($options['Email']) ? $options['Email'] : '';
		$this->phone = isset($options['Phone']) ? $options['Phone'] : '';
		$this->settings = isset($options['Settings']) ? $options['Settings'] : '';
		$this->accountStatus = isset($options['AccountStatus']) ? $options['AccountStatus'] : '';
		$this->username = isset($options['Username']) ? $options['Username'] : '';
		$this->accountType = isset($options['AccountType']) ? $options['AccountType'] : '';
		$this->dateJoined = isset($options['DateJoined']) ? $options['DateJoined'] : '';
		$this->stateId = isset($options['StateId']) ? $options['StateId'] : '';
		$this->countryId = isset($options['CountryId']) ? $options['CountryId'] : '';
		$this->cityId = isset($options['CityId']) ? $options['CityId'] : '';
		$this->website = isset($options['Website']) ? $options['Website'] :  '';
		$this->urlName = isset($options['Urlname']) ? $options['Urlname'] :  '';//this will be used for the person profile page
		$this->bioInfo = isset($options['BioInfo']) ? $options['BioInfo'] : '';
		$this->day = isset($options['DayOfBirth']) ? $options['DayOfBirth'] : '';
		$this->month = isset($options['MonthOfBirth']) ? $options['MonthOfBirth'] : '';
		$this->year = isset($options['YearOfBirth']) ? $options['YearOfBirth'] : '';
		$this->isVerified = isset($options['BlueBadge']) ? $options['BlueBadge'] : 0;
		$this->registrationStep = isset($options['RegistrationStep']) ? $options['RegistrationStep'] : '';
		$this->verificationStatus = isset($options['VerificationStatus']) ? $options['VerificationStatus'] : 0;
		$this->hasAvatar = isset($options['Avatar']) ? 'true' : 'false';
		$this->location = isset($options['Location']) ? $options['Location'] : '';
		$this->birthday = $this->arrangeBirthDay();
	  	$this->_countFollowers();

		$this->_arrangeUrls($options);

		if($this->cityId !== ''){
			$this->_fetchUserCityName($this->cityId);
		}
		if($this->stateId !== ''){
			$this->_fetchStateName($this->stateId);
		}
		if($this->countryId !== ''){
			$this->_fetchUserCountryName($this->countryId);
		}


	}

	/**
	 * Gets instance of User using application
	 *
	 * @override
	 */
	public static function getInstance($id = 0)
	{
		if(!self::$_instance) {
			if(!$id)
			{
				@self::$_instance = new self( array("id"=>$id) );
			}
			else {
				// @todo Fetch user details from DB
				$db = new DatabaseUser;
				$options = $db->fetchUser($id);
				@self::$_instance = new self($options);
			}
		}

		return self::$_instance;
	}

	/**
	 * Fetches user details from database
	 */
	protected static function findUserById($id, Database $db = null)
	{
		 $result = $this->_db->fetchUserById($id);
		 return $result;
	}

	private function _countFollowers()
	{
		$this->numbersOfFollowers = $this->_db->countUserFollowers($this->userId);
	}



	private function _fetchUserCityName($cityId)
	{
			$this->city = $this->_db->getCityNameByCityId($cityId);//get city Name
	}

	private function _fetchStateName($stateId)
	{
			$this->state = $this->_db->getStateNameByStateId($stateId);//get State Name
	}

	private function _fetchUserCountryName($countryId)
	{
			$this->country = $this->_db->getCountryNameByCountryId($countryId);//get country  Name
	}

	private function  _arrangeUrls($options)
	{
		$config = Config::getInstance();
		$uploadsConfig = $config->get('uploads');

		$this->avatar = isset($options['Avatar']) ?
		$uploadsConfig->avatarUrl.($options['Avatar']) : $uploadsConfig->avatarUrl."avatar.png";



		if (isset($options['CoverMedia'])) {
			 $this->userCoverMedia = $this->userCoverMediaType == 'video' ? $userMediaUrl . ((explode(".", $options['CoverMedia'])[0]) . "/" . ($options['CoverMedia'])) :
			  $userMediaUrl . ($options['CoverMedia']); } else {  $this->userCoverMedia = '';
		 }


	}

	/**
	* This method assigns a url to the userCoverMedia depending on the userCoverMediaType. 1 for images
	* 2 for videos
	*/
	private function setUserMediaUrl($uploadsConfig, $coverMediaType, $coverMedia)
	{
		switch ($coverMediaType)
		{
			case 1:
					$this->userCoverMediaType = 'photo' ;
				return $uploadsConfig->coverPhotoUrl;
				break;
			case 2:
				$this->userCoverMediaType = 'video' ;
				$this->posterUrl = $uploadsConfig->posters . (explode(".", $coverMedia)[0]) .".jpg";
				return $uploadsConfig->videoUrl;
				break;
			default://do nothing for now
			}
	}

	private function arrangeBirthDay()
	{

		return $this->day. "-". $this->month. "-".$this->year;
	}
}
