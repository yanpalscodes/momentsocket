<?php


class MomentPriorityCalculator
{

	private $interestScore = 0;
	private $yanpalsPartnerScore = 0;
	private $engagementScore = 0;
	private $locationScore = 0;
	private $recencyScore = 0;
	private $totalScore = 0;
	private $momentOwnerDetails;
	private $loggedInUserDetails;


	public function addMomentPriority($userId, Array $moment)
	{
		$this->userDb = new DatabaseUser();
		$this->momentDb = new DatabaseMoments();
		$this->momentOwnerDetails = $this->userDb->fetchUser($moment["UserId"]);//fetch details of Moment owner
		$this->loggedInUserDetails = $this->userDb->fetchUser($userId);

	  $this->interestScoreCalculator($userId, $moment);//calculates the score for moment based on interest of logged in user
		$this->yanpalsPartnerScoreCalculator($this->momentOwnerDetails);//calculates score based on the condition if the moment owner is a member of yanpals network
		$this->engagementScoreCalculator($moment);//calculates score based on the rebroadcasting counts on the moments
		$this->recencyScoreCalculator($moment);// calculates score based on the time the moment was made
		$this->locationScoreCalculator($this->loggedInUserDetails, $this->momentOwnerDetails);//calculates point based on the location of the moment owner and logged in user

		return  $this->interestScore + $this->yanpalsPartnerScore + $this->engagementScore + $this->recencyScore +
		$this->locationScore;

	;

	}


	private function interestScoreCalculator($userId, $moment)
	{
		$loggedInUserInterestFromDb = $this->userDb->fetchUserInterests($userId);//fetch user interest
		$momentInterestFromDb = 	$this->momentDb->fetchMomentIntersts($moment["MomentId"]);//fetch moment interest
		if(count($momentInterestFromDb) > 0  && count($loggedInUserInterestFromDb ) > 0){

					foreach($loggedInUserInterestFromDb as $oneUserInterest){

						 foreach ($momentInterestFromDb as $key) {
						 	 if($oneUserInterest == $key){
								 $this->interestScore = 40;
								 break;
							 }
						 }
					}
				}
				else{
					 $this->interestScore = 0;
				}

					//echo $this->interestScore;
	}


	private function yanpalsPartnerScoreCalculator($momentOwnerDetails)
	{
		//var_dump($momentOwnerDetails);
	  ($momentOwnerDetails->IsPartner == 1)? $this->yanpalsPartnerScore = 10 : $this->yanpalsPartnerScore;

	}

	private function engagementScoreCalculator($moment)
	{
			//based on formular formed by Engineer Obot, 10 is a constant
			$rebroadcastCount = $this->momentDb->countNumberOfRebroadcastForAMoment($moment["MomentId"]);
			$engagementFraction = (10) / (exp($rebroadcastCount));
			$this->engagementScore = 10 - $engagementFraction;

	}

	private function recencyScoreCalculator($moment)
	{
		$timeLapse =  (time() - $moment["Time"]);
		$oneDayInSeconds = 24*60*60;
		$numberOfDays = ceil($timeLapse/$oneDayInSeconds);
		//an = 7 + ((n-1) * (-1)).....7 is a constant
		$recency = 7 + (($numberOfDays - 1)  * -1);
		($recency > 0)? $this->recencyScore = $recency : $this->recencyScore = 0;

	}


	private function locationScoreCalculator($loggedInUserDetails, $momentOwnerDetails)
	{
		if(($loggedInUserDetails->CountryId != NULL) &&  ($loggedInUserDetails->CountryId  == $momentOwnerDetails->CountryId)){
			$this->locationScore = 1;
		}
		if(($loggedInUserDetails->StateId != NULL) &&  ($loggedInUserDetails->StateId  == $momentOwnerDetails->CountryId)){
			$this->locationScore = 5;
		}
		if(($loggedInUserDetails->CityId != NULL) &&  ($loggedInUserDetails->CountryId  == $momentOwnerDetails->CityId)){
			$this->locationScore = 10;
		}
	}






}
