<?php
/**
 * Loader
 */
define('_PUBLIC', 1);
define('DIR_ROOT', __DIR__);

// Events
require_once DIR_ROOT . '/lib/Event/EventInterface.php';
require_once DIR_ROOT . '/lib/Event/Event.php';
require_once DIR_ROOT . '/lib/Event/ObserverInterface.php';
require_once DIR_ROOT . '/lib/Event/Observer.php';
require_once DIR_ROOT . '/lib/Event/TargetInterface.php';
require_once DIR_ROOT . '/lib/Event/Target.php';

// WebSocket Event
require_once DIR_ROOT . '/lib/WebSocket/Event/Event.php';
require_once DIR_ROOT . '/lib/WebSocket/Event/Observer.php';
require_once DIR_ROOT . '/lib/WebSocket/Event/Target.php';


// WebSocket
require_once DIR_ROOT . '/lib/WebSocket/Exception.php';
require_once DIR_ROOT . '/lib/WebSocket/Logger.php';
require_once DIR_ROOT . '/lib/WebSocket/Handshake/Request.php';
require_once DIR_ROOT . '/lib/WebSocket/Handshake/Response.php';
require_once DIR_ROOT . '/lib/WebSocket/Server.php';
require_once DIR_ROOT . '/lib/WebSocket/Client.php';



//Vendors
require_once DIR_ROOT . '/vendors/Unirest/Exception.php';
require_once DIR_ROOT . '/vendors/Unirest/Method.php';
require_once DIR_ROOT . '/vendors/Unirest/Response.php';
require_once DIR_ROOT . '/vendors/Unirest/Request.php';
require_once DIR_ROOT . '/vendors/Unirest/Request/Body.php';
//require_once DIR_ROOT . '/vendors/Unirest/Unirest';


//
require_once DIR_ROOT . '/application/Mainsocket/loader.php';
require_once DIR_ROOT . '/application/Mainsocket/CentralController.php';
require_once DIR_ROOT . '/application/Mainsocket/CreateMomentController.php';
require_once DIR_ROOT . '/application/Mainsocket/MomentCategoriationAi.php';
