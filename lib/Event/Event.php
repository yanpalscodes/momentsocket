<?php
/**
 *
 */
namespace Mf_Core\Event;
use Mf_Core\Event\EventInterface;
use Mf_Core\Event\Target;

/**
 * Mf_Core\Event\Event
 */
class Event implements EventInterface
{
	// Event type
	protected $_type;

	// Source object
	protected $_target;
	
	// @var 	Time event was created
	protected $_timestamp;
	
	// Parameters passed to observer
	protected $_params = array();
	
	/**
	 *
	 */
	public function __construct($type, array $params = array())
	{
		$this->_timestamp = time();
		$this->_type = $type;

		// Set params
		foreach($params as $key => $value)
		{
			$this->setParam($key, $value);
		}
	}
	
	/**
	 *
	 */
	public function setTarget(Target $target)
	{
		$this->_target = $target;
	}
	
	/**
	 *
	 */
	public function getTarget()
	{
		return $this->_target;
	}
	
	/**
	 *
	 */
	public function getType()
	{
		return $this->_type;
	}
	
	/**
	 *
	 */
	public function getTimestamp()
	{
		return $this->_timestamp;
	}
	
	/**
	 *
	 */
	public function setParam($key, $value)
	{
		$this->_params[$key] = $value;
	}
	
	/**
	 *
	 */
	public function getParam($key)
	{
		$value = null;
		if(isset($this->_params[$key]))
		{
			$value = $this->_params[$key];
		}
		
		return $value;
	}
	
	/**
	 *
	 */
	public function getParams()
	{
		return $this->_params;
	}
}
