<?php
/**
 *
 */
namespace Mf_Core\Event;
use Mf_Core\Event\Event;

/**
 *
 */
Interface ObserverInterface
{
	public function update(Event $event);
}