<?php
/**
 * Mule Framework (WebSocket)
 */
namespace Mf\WebSocket;
use \Exception as CoreException;

/**
 *
 */
class Exception extends CoreException
{
	/**
	 *
	 */
	public function __construct($message, $code = 0)
	{
		parent::__construct($message, $code);
	}
}
